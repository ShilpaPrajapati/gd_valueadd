# Value Ad Mobile Repository
This document serves as a getting started guide for any developer who wish to contribute to the project.
## Table of Contents
1. [Getting Started](#getting-started)
### Getting Started
This is a cross platform mobile app and mobile website, build in [Cordova](https://cordova.apache.org/) for Android and iOS. The project uses [Jquery Mobile](https://jquerymobile.com/) as display framework. 

#### Requirements
1. [Node JS](https://nodejs.org/en/)
2. [Cordova](https://cordova.apache.org/)
3. [Android Studio](https://developer.android.com/studio/index.html)
4. xCode

#### Installation
Install version 6.x of NodeJs on the desired development machine. 
After installed, run the following commands:

```
npm install cordova
```

1. Checkout the git solution
2. Go into the project folder and open a terminal

Run: 

```
cordova platform rm ios
```
```
cordova platform rm android
```
```
cordova platform add ios
```
```
cordova platform add android
```

To install the below plugins use the git url eg:
```
cordova plugin add https://github.com/Telerik-Verified-Plugins/PushNotification.git
```
#### Plugins
The below plugins is used for the Cordova App
1. [com.phonegap.plugins.PushPlugin](https://github.com/Telerik-Verified-Plugins/PushNotification.git)
2. [com.phonegap.plugins.twilioclient](https://github.com/jefflinwood/twilio_client_phonegap.git)
3. [cordova-plugin-console](https://github.com/apache/cordova-plugin-console.git)
4. [cordova-plugin-device](https://github.com/apache/cordova-plugin-device.git)
5. [cordova-plugin-globalization](https://github.com/apache/cordova-plugin-globalization.git)
6. [cordova-plugin-inappbrowser](https://github.com/apache/cordova-plugin-inappbrowser.git)
7. [cordova-plugin-whitelist](https://github.com/apache/cordova-plugin-whitelist.git)
8. [cordova-plugin-statusbar](https://github.com/apache/cordova-plugin-statusbar.git)

#### Run the project
To compile the project, run the following commands:

Android will generate an APK file in the platforms folder created the Android platform add command

```
cordova build android --release
```
iOS will update the xcode project in the platforms folder from where the solution can be opened in xCode

```
cordova prepare ios
```



