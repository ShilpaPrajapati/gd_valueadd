var _init =
{
    init: function(initReset)
    {
        gen.VERSION = "V @VERSION@";
        gen.DEV = false;

        gen.COMMON_JS = "js/valuead/common.js";
        gen.CLIENTS_JS = "js/valuead/clients.js";
        gen.LOGIN_JS = "js/valuead/login.js";
        gen.CAMPAIGNS_JS = "js/valuead/campaigns.js";
        gen.LEADS_JS = "js/valuead/leads.js";
        gen.FEEDBACK_JS = "js/valuead/feedback.js";
        gen.TWILIO_JS = "js/valuead/twilio.js";
        gen.NOTIFICATIONS_JS = "js/valuead/notifications.js";

        gen.COMMON_XML = "xml/common.xml";
        gen.CAMPAIGNS_XML = "xml/campaigns.xml";
        gen.LEADS_XML = "xml/leads.xml";
        gen.LOCALIZATION_XML = "xml/localization.xml";

        gen.S3_BUCKET = "https://s3-ap-southeast-2.amazonaws.com/pgva";
        gen.S3_VERSION = "2_0";

        gen.AUTO_LOGOUT = 15;

            _common.init();

            if (initReset == true)
                setTimeout(_reset.init, 1000);
    }
};
