var _campaigns = {

    preInit: function () {
        $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
            options.data = jQuery.param($.extend(originalOptions.data || {}, {
                timeStamp: new Date().getTime()
            }));
        });
         // Get the Campaigns.
        this.CampaignsListGD();
    },

    CampaignsListGD: function () {
        var method = "POST";
        var url = gen.common.PROXY;
        var timeout = 30;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;
        data = {
            token: gen.token.access_token,
            refreshToken: gen.token.refresh_token,
            method: "GET",
            action: "campaigns",
            base: true,
            data: ""
        }
       var compaignRequest = window.plugins.GDHttpRequest.createRequest(method, url, timeout, isAsync, user, password, auth, isIncremental);
        compaignRequest.addRequestHeader("contentType", "application/x-www-form-urlencoded");
        compaignRequest.addRequestHeader("cache-control", "no-cache");
        compaignRequest.addRequestHeader("Accept-Language", gen.LanguageCode);
        compaignRequest.addPostParameter("callback", "getdata");
        compaignRequest.addPostParameter("token", gen.token.access_token);
        compaignRequest.addPostParameter("refreshToken", gen.token.refresh_token);
        compaignRequest.addPostParameter("method", "GET");
        compaignRequest.addPostParameter("action", "campaigns");
        compaignRequest.addPostParameter("base", "true");
        compaignRequest.addPostParameter("data", "");
        compaignRequest.disablePeerVerification = false;
        compaignRequest.disableHostVerification = false;
        function sendSuccess(response) {
            console.log("Received valid response from the send request");
            try {
                //-- parseHttpResponse
                var responseObj = window.plugins.GDHttpRequest.parseHttpResponse(response);
                $.mobile.loading('hide', {});
                
                  var resonsedata = responseObj.responseText.toString();
                resonsedata = resonsedata.replace("getdata(", "").replace(")", "");
                resonsedata=JSON.parse(resonsedata);
                if (resonsedata.error) {
                    $("#leadsSignOutButton").trigger("click");
                } else {
                    gen.campaigns = resonsedata.campaigns;
                    gen.selectedCampaign = gen.campaigns[0];
                    //GD not support getresponseheader so commented that code
                    // var newAccessTokenHeader = compaignRequest.getResponseHeader('newAccessToken');
                    //     alert("new token"+newAccessTokenHeader);
                    // if (newAccessTokenHeader)
                    //     gen.token.access_token = newAccessTokenHeader;
                 _campaigns.init();
                }

            } catch (e) {
                $.mobile.loading('hide', {});
                _common.hidePreLoaders();
                $("#leadsSignOutButton").trigger("click");
            }
        };

        function sendFail() {
            $.mobile.loading('hide', {});
            _common.hidePreLoaders();
            $("#leadsSignOutButton").trigger("click");
        }
        compaignRequest.send(sendSuccess, sendFail);

    },
    init: function () {
        this.ui = {};
        this.ui.captureButton = $("#captureButton");
        this.ui.helpButton = $("#helpButton");
        this.ui.leadsListButton = $("#leadsListButton");
        this.ui.campaignSelectMenu = $("#campaignSelectMenu");
        this.ui.campaignsSignOutButton = $("#campaignsSignOutButton");
        this.ui.campaignsSignOutButton.off().on("click", $.proxy(this.onSignOutClick, this));
        this.ui.dashboardButton = $("#dashboardButton");
        if (gen.common.DashboardURL != "")
            this.ui.dashboardButton.off().on("click", $.proxy(this.onDashboardClick, this));
        else
            this.ui.dashboardButton.hide();

        this.ui.campaignSelectMenu.empty();
        this.buildCampaignList(gen.FirstLoad);

        $("#campaigns_image").css("background-image", gen.CampaignsHeaderImage);
        _common.displayLanguage();

        var languageSelectContainer = $("#languageSelectContainerCampaigns");

        if (gen.common.EnableGlobalization) {
            languageSelectContainer.show();
        } else {
            languageSelectContainer.hide();
        }

        //If all our help options is disabled in common.xml, then hide help button.
        if (!gen.common.helpQuestionURL && !gen.common.helpSuggestionAddress && !gen.common.helpBugAddress) {
            this.ui.helpButton.hide();
        }

        // Seperate if statements due to complexity being added
        if (!gen.common.PHONEGAP) {
            this.ui.helpButton.hide();
        }
    },

    onDashboardClick: function () {
        var fullURL = gen.common.DashboardURL + gen.LoggedInUserId;
        window.open(fullURL, '_blank');
    },

    onSignOutClick: function () {
        
        _common.resetTokens();
    },

    buildCampaignList: function (executeAutoSelect) {
        this.ui.campaignSelectMenu.empty();
        this.appendData(gen.campaigns, this.ui.campaignSelectMenu, executeAutoSelect);
        $.mobile.loading('hide', {});
        gen.FirstLoad = false;
    },

    appendData: function (data, $holder, executeAutoSelect) {
        if (data.length == 0) {
            $holder.append('<li value="-1">No Campaigns Available</li>');
            return;
        }

        //• LIST CAMPAIGNS AS PER campaigns.xml
        $.each(gen.CampaignConfiguration.Campaigns.Campaign, function (i, item) {
            if (item.Visible == "true") {
                var campaignID = item.Id;

                for (var i = 0; i < data.length; i++) {
                    var campaignItem = data[i];

                    if (campaignItem.id == parseInt(campaignID)) {
                        gen.selectedCampaignConfiguration = item;
                        var campaignDescription = campaignItem.name;
                        var campaignMessage = "";
                        var campaignAllowsCapture = false;
                        if (item.DescriptionLocalizationKey)
                            campaignDescription = _common.getLocalization(item.DescriptionLocalizationKey, campaignItem.name);

                        if (item.MessageLocalizationKey)
                            campaignMessage = _common.getLocalization(item.MessageLocalizationKey, campaignItem.name);

                        if (item.AllowCapture)
                            campaignAllowsCapture = item.AllowCapture;

                        h = "";
                        h = '<li data-value="' + campaignItem.id + '" title="' + campaignMessage + '" data-capture="' + campaignAllowsCapture + '" data-theme-b><a>';
                        h += campaignDescription;
                        h += '<span class="ui-li-count">' + campaignItem.activeLeads + ' ' + _common.getLocalization('active', 'Active') + '</span></a></li>';

                        $holder.append(h);
                        break;
                    }
                }
            }
        })

        this.ui.campaignSelectMenu.listview().listview('refresh');
        this.ui.campaignSelectMenu.find('li').off().on("click", $.proxy(this.onCampaignSelect, this));

        this.ui.captureButton.off().on("click", $.proxy(this.onCaptureClick, this));
        this.ui.leadsListButton.off().on("click", $.proxy(this.onLeadsListClick, this));

        //• LINK NAVIGATION
        if (gen.LinkNavigation != null && gen.LinkNavigation.Campaign != null) {
            this.onAutoCampaignSelect(gen.LinkNavigation.Campaign);
            gen.LinkNavigation.Campaign = null;
        } else {
            //• AUTO-SELECT IF THERE IS ONLY 1 VISIBLE CAMPAIGN
            if (executeAutoSelect && _campaigns.visibleCampaigns() == 1) {
                gen.selectedCampaign = _campaigns.visibleCampaign(data);
                gen.selectedCampaignAllowsAdding = _campaigns.campaignAllowsCapture(gen.selectedCampaign.id);
                _campaigns.onAutoCampaignSelect(gen.selectedCampaign.id);
            }
        }

        _common.displayLanguage();
    },

    passOnCampaigns: function () {
        var result = 0;
        for (var i = 0; i < gen.CampaignConfiguration.Campaigns.Campaign.length; i++) {
            if (gen.CampaignConfiguration.Campaigns.Campaign[i].AllowPassOn && gen.CampaignConfiguration.Campaigns.Campaign[i].AllowPassOn === "true")
                result++;
        }
        return result;
    },

    visibleCampaigns: function () {
        var result = 0;
        for (var i = 0; i < gen.CampaignConfiguration.Campaigns.Campaign.length; i++) {
            if (gen.CampaignConfiguration.Campaigns.Campaign[i].Visible === "true")
                result++;
        }
        return result;
    },

    visibleCampaign: function (campaigns) {
        var result = 0;
        for (var i = 0; i < gen.CampaignConfiguration.Campaigns.Campaign.length; i++) {
            if (gen.CampaignConfiguration.Campaigns.Campaign[i].Visible === "true") {
                for (var ii = 0; ii < campaigns.length; ii++) {
                    if (campaigns[ii].id == gen.CampaignConfiguration.Campaigns.Campaign[i].Id)
                        return campaigns[ii];
                }
            }
        }
    },

    campaignAllowsCapture: function (campaignID) {
        for (var i = 0; i < gen.CampaignConfiguration.Campaigns.Campaign.length; i++) {
            if (gen.CampaignConfiguration.Campaigns.Campaign[i].Id == campaignID) {
                if (gen.CampaignConfiguration.Campaigns.Campaign[i].AllowCapture)
                    return gen.CampaignConfiguration.Campaigns.Campaign[i].AllowCapture;
                else
                    return false;
            }
        }
    },

    onCampaignSelect: function (e) {
        var $target = $(e.srcElement || e.target)
        gen.selectedCampaign = null;
        this.campaignByID($target.closest("li").attr("data-value"));
        gen.selectedCampaignAllowsAdding = $target.closest("li").attr("data-capture");
        this.onLeadsListClick();
    },

    onAutoCampaignSelect: function (campaignID) {
        this.campaignByID(campaignID);
        setTimeout(_campaigns.onLeadsListClick, 250);
    },

    campaignByID: function (campaignID) {
        var row = gen.campaigns.findByField("id", campaignID);
        if (row == null) return;
        gen.selectedCampaign = row;
    },

    onCaptureClick: function () {
        if (gen.selectedCampaign != null)
            $("#newLead_Page").trigger("click");
    },

    onLeadsListClick: function () {
        if (gen.selectedCampaign != null) {
            $("#campaignMessageContainer").hide();
            $("#navToLeads").trigger("click");

            var loading = _common.getLocalization('loading', 'Loading ... ')

            $.mobile.loading('show', {
                text: loading,
                textVisible: true,
                theme: "a",
                html: ""
            });
            _leads.preInit();
        }
    }
};