var _clients =
{
    init: function ()
    {
        this.ui = {};
        this.ui.clientCode = $("#clientCode");
        this.ui.setClientButton = $("#setClientButton");
        this.ui.setClientButton.fadeIn();
        this.ui.clientCode.off().on("keypress", $.proxy(this.onClientCodeEnter, this));
        this.ui.setClientButton.off().on("click", $.proxy(this.onClientButtonClick, this));
        _common.displayLanguage();
    },

    onClientCodeEnter: function ()
    {
        if (event.which == 13)
        {
            event.preventDefault();
            this.onClientButtonClick();
        }
    },

    onClientButtonClick: function ()
    {
        if (this.ui.clientCode.val() === "")
        {
            var noCode = _common.getLocalization("noCode", "You have not entered a Client Code.");
            _common.showNotification(noCode);
            return;
        }

        _clients.validateClient(this.ui.clientCode.val());
        if (gen.common.FOLDER != null && gen.common.FOLDER !== "")
        {
            localStorage.setItem('folder', gen.common.FOLDER);
            localStorage.setItem('proxy', gen.common.PROXY);
            localStorage.setItem('reset_page', gen.common.RESET_PAGE);
            _common.goToLogin();
        }
        else
        {
            var invalidCode = _common.getLocalization("invalidCode", "You have entered an Invalid Client Code.");
            _common.showNotification(invalidCode);
            return;
        }
    },

    validateClient: function(code)
    {
        for (var counter = 0; counter < gen.Clients.Client.length; counter++)
        {
            var client = gen.Clients.Client[counter];
            if (client.Active == "true" && client.Code.toUpperCase() == code.toUpperCase())
            {
                gen.common.FOLDER = client.Folder;
                gen.common.PROXY = client.Proxy;
                gen.common.RESET_PAGE = client.ResetPage;
                return;
            }
        }
    }
};