#
# (c) 2015 Good Technology Corporation. All rights reserved.
#

#!/bin/bash

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
	
	echo 
	PROCESSES=$(ps aux | grep -e 'adb.*logcat\|tail -f*' | grep -v grep | awk '{print $2}')
	for i in $PROCESSES; do
		kill -9 $i >/dev/null
	done
}

# $1 - file name to log output

function printHelp {
	echo ""
	echo "Usage: $0 [OutputFile]"
    echo ""
    echo "OutputFile is the path/name of file to log output."
    echo "It's an optional parameter and if missing a a file in the current location will be created"
    echo ""
}

while getopts ":h" OPTION
do
    case $OPTION in
    h)  printHelp
        exit 1
        ;;
    esac
done

# retrieve device identifiers
DEVICES=($(adb devices | awk '$2 ~ /device/ { print $1}'))
if [ ${#DEVICES[@]} -lt 2 ]; then
	echo "Connect both devices to your computer"
	printHelp
	
	exit 1
fi

# check if an output file has been specified or create one if not
OUTPUT_FILE=$1
if [ -z "$OUTPUT_FILE" ]; then
	DATE=$(date +"%Y%m%d%H%M")
	OUTPUT_FILE='combined_logcat_'$DATE'.log'
	echo "No OutputFile specified, using the default one $OUTPUT_FILE"
fi

# first clear logcat buffers on device so only capture log which is needed
for device in ${DEVICES[@]} ; do
	adb -s ${device} logcat -c
done

#log output from both devices to same log file. Logs are this interleaved
COMMAND_STRING="(cat <("
COMMAND_SEPARATOR=''
OUTPUT_FILE_DEV=''
for device in ${DEVICES[@]} ; do
	if [ -z "$COMMAND_SEPARATOR" ]; then
		COMMAND_SEPARATOR=" & "
	else 
		COMMAND_STRING+=$COMMAND_SEPARATOR
	fi
	
	OUTPUT_FILE_DEV="${OUTPUT_FILE%%.*}_${device}.${OUTPUT_FILE#*.}"
	COMMAND_STRING+="(adb -s "
	COMMAND_STRING+=${device}
	COMMAND_STRING+=" logcat -v threadtime > '"
	COMMAND_STRING+="${OUTPUT_FILE_DEV}"
	COMMAND_STRING+="' & tail -f '"
	COMMAND_STRING+="${OUTPUT_FILE_DEV}"
	COMMAND_STRING+="' | while read line; do echo ${device} \$line ; done)"
done

COMMAND_STRING+="))"

eval $COMMAND_STRING > "$OUTPUT_FILE"
