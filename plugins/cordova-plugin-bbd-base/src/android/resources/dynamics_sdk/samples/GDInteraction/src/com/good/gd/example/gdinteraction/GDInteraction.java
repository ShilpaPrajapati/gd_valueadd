/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.gdinteraction;

import static com.good.gd.GDAppResultCode.GDErrorActivationFailed;
import static com.good.gd.GDAppResultCode.GDErrorAppDenied;
import static com.good.gd.GDAppResultCode.GDErrorBlocked;
import static com.good.gd.GDAppResultCode.GDErrorIdleLockout;
import static com.good.gd.GDAppResultCode.GDErrorPasswordChangeRequired;
import static com.good.gd.GDAppResultCode.GDErrorProvisioningFailed;
import static com.good.gd.GDAppResultCode.GDErrorPushConnectionTimeout;
import static com.good.gd.GDAppResultCode.GDErrorRemoteLockout;
import static com.good.gd.GDAppResultCode.GDErrorSecurityError;
import static com.good.gd.GDAppResultCode.GDErrorWiped;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ScrollView;
import android.widget.TextView;

import com.good.gd.GDAndroid;
import com.good.gd.GDAppEvent;
import com.good.gd.GDAppEventType;
import com.good.gd.GDAppResultCode;
import com.good.gd.GDStateListener;
import com.good.gd.error.GDError;

/**
 * GDInteraction activity; shows all incoming events, plus internal events like onPause,
 * onResume, and when buttons are clicked.
 */
public class GDInteraction extends Activity implements GDStateListener,
		OnClickListener {

	private TextView _statusTextView;
	private ScrollView _scroller;
	private DateFormat _timeFormatter;
	private GDEventHandler _gdEventHandler;
	private int numEventsLogged = 0;

	public GDInteraction() {
		super();
		_timeFormatter = new SimpleDateFormat("HH:mm:ss");
		_gdEventHandler = GDEventHandler.getInstance();

		// singleton GD Event Handler registered with GDAndroid
		GDAndroid.getInstance().setGDAppEventListener(_gdEventHandler);
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		GDAndroid.getInstance().activityInit(this);
		setContentView(R.layout.main);

		// link to the various elements of the main view

		_statusTextView = (TextView) findViewById(R.id.gdinteraction_status_view);
		_scroller = (ScrollView) findViewById(R.id.gdinteraction_scroller);

		GDEventHandler.getInstance().setGDInteractionActivity(this);
	}

	@Override
	protected void onPause() {
		super.onPause();

		logStatus("onPause()");
		logStatus("");
	}

	@Override
	public void onResume() {
		super.onResume();

		logStatus("onResume()");
		logStatus("");

		updateFromEventHandler();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	// Ask the GDEventHandler for all the events it has ever received, and log
	// all those
	// not already logged. This means that if the Activity is restarted, it will
	// correctly
	// log everything from when the app was started.
	@SuppressWarnings("unchecked")
	void updateFromEventHandler() {

		for (; numEventsLogged < _gdEventHandler.eventCount(); numEventsLogged++) {
			GDAppEvent anEvent = _gdEventHandler.eventAtIndex(numEventsLogged);
			GDAppEventType eventType = anEvent.getEventType();

			if (eventType == GDAppEventType.GDAppEventRemoteSettingsUpdate) {

				logStatus("GDAppEventListener Received SettingsUpdate event: "
						+ anEvent);
				Map<String, Boolean> protocols = (Map<String, Boolean>) GDAndroid.getInstance().getApplicationConfig()
						.get(GDAndroid.GDAppConfigKeyCommunicationProtocols);
				StringBuilder allowedCommunicationProtocolsString = new StringBuilder("Allowed communication protocols:\n");
				allowedCommunicationProtocolsString.append(GDAndroid.GDProtocolsKeyTLSv1_0 + ": " + protocols.get(GDAndroid.GDProtocolsKeyTLSv1_0) + "\n");
				allowedCommunicationProtocolsString.append(GDAndroid.GDProtocolsKeyTLSv1_1 + ": " + protocols.get(GDAndroid.GDProtocolsKeyTLSv1_1) + "\n");
				allowedCommunicationProtocolsString.append(GDAndroid.GDProtocolsKeyTLSv1_2 + ": " + protocols.get(GDAndroid.GDProtocolsKeyTLSv1_2) + "\n");
				allowedCommunicationProtocolsString.append(GDAndroid.GDProtocolsKeySSLv3_0 + ": " + protocols.get(GDAndroid.GDProtocolsKeySSLv3_0));
				logStatus(allowedCommunicationProtocolsString.toString());

			} else if (eventType == GDAppEventType.GDAppEventAuthorized) {

				logStatus("GDAppEventListener Received authorized event: "
						+ anEvent);

			} else if (eventType == GDAppEventType.GDAppEventNotAuthorized) {

				logStatus("GDAppEventListener Received unauthorized event: "
						+ anEvent);

				GDAppResultCode resultCode = anEvent.getResultCode();
				String explanation = (resultCode == GDErrorActivationFailed) ? "activation not completed"
						: (resultCode == GDErrorProvisioningFailed) ? "activation not completed"
								: (resultCode == GDErrorPushConnectionTimeout) ? "activation could not be completed"
										: (resultCode == GDErrorIdleLockout) ? "application blocked till unlocked"
												: (resultCode == GDErrorRemoteLockout) ? "application blocked till unlocked"
														: (resultCode == GDErrorPasswordChangeRequired) ? "application blocked till password changed"
																: (resultCode == GDErrorSecurityError) ? "secure store could not be unlocked"
																		: (resultCode == GDErrorBlocked) ? "application blocked"
																				: (resultCode == GDErrorAppDenied) ? "container is wiped"
																						: (resultCode == GDErrorWiped) ? "container is wiped"
																								: "INVALID RESULT CODE";
				logStatus("- " + explanation);

			} else if (eventType == GDAppEventType.GDAppEventServicesUpdate) {

				logStatus("GDAppEventListener Received service update event: "
						+ anEvent);

			} else {
				logStatus("GDAppEventListener Got unknown event: " + anEvent);
			}

			logStatus("");
		}
	}

	// Write the specified message to the on-screen log (with timestamp), and
	// also to the android log
	private void logStatus(String message) {
		if (_statusTextView != null) {
			String outputmessage = (message.length() > 0) ? _timeFormatter
					.format(new Date()) + " " + message + "\n" : "\n";
			_statusTextView.append(outputmessage);
			_scroller.post(new Runnable() {
				@Override
				public void run() {
					_scroller.fullScroll(View.FOCUS_DOWN);
				}
			});
		}
		Log.v(this.getClass().getPackage().getName(), message + "\n");
	}

	@Override
	public void onAuthorized() {

		logStatus("GDStateListener Received GD authorized event");
	}

	@Override
	public void onLocked() {
		logStatus("GDStateListener Received GD locked event - GD APIs can be used but user interaction can't be used");

	}

	@Override
	public void onWiped() {
		logStatus("GDStateListener Received GD wiped event - GD APIs can't be used");
	}

	@Override
	public void onUpdatePolicy(Map<String, Object> policyValues) {
		logStatus("GDStateListener onUpdatePolicy - New Policy Settings received");
		for (String key : policyValues.keySet()) {
			logStatus("  " + key + "=" + policyValues.get(key));
		}
	}

	@Override
	public void onUpdateServices() {
		logStatus("GDStateListener onUpdateServices");

	}

    @Override
    public void onUpdateDataPlan() {
        logStatus("GDStateListener onUpdateDataPlan");

    }

	@Override
	public void onUpdateConfig(Map<String, Object> settings) {
		logStatus("GDStateListener onUpdateConfig - New App Config received");
		for (String key : settings.keySet()) {
			logStatus("  " + key + "=" + settings.get(key));
		}

	}

    @Override
    public void onUpdateEntitlements() {
        logStatus("GDStateListener onUpdateEntitlements");
        
    }

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.action_getappconfig:
			actionGetAppConfig();
			break;
		case R.id.action_change_password:
			actionChangePassword();
			break;
		case R.id.action_quit:
			logStatus("Quit pressed; Calling finish()");
			finish();
		default:
			break;
		}

	}

	private void actionGetAppConfig() {
		logStatus("Calling GDAndroid.getApplicationConfig()");
		try {
			Map<String, Object> settings = GDAndroid.getInstance()
					.getApplicationConfig();
			logStatus("Values returned by getApplicationConfig:");
			for (String key : settings.keySet()) {
				logStatus("  " + key + "=" + settings.get(key));
			}
		} catch (GDError e) {
			logStatus("Error thrown by getApplicationConfig() call: " + e);
		}
	}

	private void actionChangePassword() {
		logStatus("Calling GDAndroid.openChangePasswordUI()");
		try {
			GDAndroid.getInstance().openChangePasswordUI();
		} catch (GDError e) {
			logStatus("Error thrown by openChangePasswordUI() call: " + e);
		}
	}
}
