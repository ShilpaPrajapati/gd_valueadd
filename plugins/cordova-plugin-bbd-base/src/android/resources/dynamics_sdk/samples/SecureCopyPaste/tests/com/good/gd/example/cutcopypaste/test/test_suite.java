/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2016 Good Technology Corporation. All rights reserved.
 */
package com.good.gd.example.cutcopypaste.test;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.good.automated_test_support.GDAutomatedTestSupport;
import com.good.gd.GDAndroid;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;

@RunWith(AndroidJUnit4.class)
public class test_suite {

    @BeforeClass
    public static void setUpClass() {

        //Setup test support and register a GDStateListener
        GDAutomatedTestSupport.setupGDAutomatedTestSupport();

        GDAutomatedTestSupport.wakeUpDeviceIfNeeded();

        //Android Emulator when booted sometimes has error dialogues to dismiss
        GDAutomatedTestSupport.acceptSystemDialogues();

    }

    /*
    Test 1, if GD App is already activated ensure that it can be unlocked using password. If not activated ensure it
    can be activated using Email Address & Access Key. These are set in file com.good.gd.test.json at build time
    */
    @Test
    public void test_1_GDActivateSecureCopyPaste( ) throws Exception {

        GDAutomatedTestSupport.launchAppUnderTest();

        assertTrue(GDAutomatedTestSupport.loginOrProvisionGDApp());

        assertTrue(GDAutomatedTestSupport.checkGDAuthorized());

        GDAutomatedTestSupport.pressHome();
    }

    //Further App specific tests should be added here

    @After
    public void tearDown() throws Exception {

    }
}