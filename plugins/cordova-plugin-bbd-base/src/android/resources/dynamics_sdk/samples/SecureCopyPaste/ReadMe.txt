### SecureCopyPaste ###

================================================================================
DESCRIPTION:
This is a sample application that shows comparison between usage of secured GD
UI text controls (GDTextView, GDEditText, GDAutoCompleteTextView, GDSearchView,
GDWebView) and corresponding Android default UI text controls. In GD text
controls, text data is encrypted or decrypted before copy/paste operations.

For more examples visit the Good Developer Network:
https://begood.good.com/community/gdn

Support:
https://begood.good.com/community/gdn/support
================================================================================
(c) 2015 Good Technology Corporation. All rights reserved.