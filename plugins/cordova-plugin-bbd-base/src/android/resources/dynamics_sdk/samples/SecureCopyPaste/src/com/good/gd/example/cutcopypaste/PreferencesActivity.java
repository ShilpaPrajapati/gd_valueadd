package com.good.gd.example.cutcopypaste;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.good.gd.GDAndroid;
import com.good.gd.GDStateListener;

import java.util.Map;

/**
 * Example Activity to show the Secure GDEditTextPreference being used as part of the Android Preferences
 * framework. As it is a Secure entity it allows (a) dictation to be disabled (b) Secure Copy/Paste to be used
 */
public class PreferencesActivity extends Activity implements GDStateListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        GDAndroid.getInstance().activityInit(this);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new PrefsFragment()).commit();
    }

    @Override
    public SharedPreferences getSharedPreferences(String name, int mode) {

        /*
        We use the GD Secure SharedPreferences to ensure everything the preferences screen saves is
        Securely saved by the GDSharedPreferences class
         */
        return GDAndroid.getInstance().getGDSharedPreferences(name, mode);
    }

    @Override
    public void onAuthorized() {

    }

    @Override
    public void onLocked() {

    }

    @Override
    public void onWiped() {

    }

    @Override
    public void onUpdateConfig(Map<String, Object> settings) {

    }

    @Override
    public void onUpdatePolicy(Map<String, Object> policyValues) {

    }

    @Override
    public void onUpdateServices() {

    }

    @Override
    public void onUpdateDataPlan() {

    }

    @Override
    public void onUpdateEntitlements() {

    }

    public static class PrefsFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.preferences);
        }
    }

}
