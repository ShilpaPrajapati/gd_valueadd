/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2014 Good Technology Corporation. All rights reserved.
 */
package com.good.gd.example.securestore;

import android.app.Application;


public class SecureStoreApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Create & initialize app control singletons
        AppStateManager.createInstance(getApplicationContext());

    }
}
