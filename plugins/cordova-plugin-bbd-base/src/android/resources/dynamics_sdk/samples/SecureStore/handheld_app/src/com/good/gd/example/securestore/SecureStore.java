/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.securestore;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ToggleButton;

import com.good.gd.GDAndroid;

import com.good.gd.example.securestore.common_lib.AppGDStateControl;
import com.good.gd.example.securestore.common_lib.AppGDStateControlListener;
import com.good.gd.example.securestore.common_lib.ConnectedApplicationControl;
import com.good.gd.example.securestore.common_lib.ConnectedApplicationListener;
import com.good.gd.example.securestore.common_lib.ConnectedApplicationState;

import static com.good.gd.example.securestore.common_lib.utils.AppLogUtils.DEBUG_LOG;


/**
 * SecureStore activity - a basic file browser list which supports multiple modes
 * (Container and insecure SDCard). Files can be deleted, moved to the container
 * and if they're .txt files they can be opened and viewed.
 */
public class SecureStore extends Activity implements OnClickListener, AppGDStateControlListener, ConnectedApplicationListener {

    protected FileBrowserFragment m_fragment = null;

    private final String FRAGMENT_TAG = "Secure_Store_UI";

    private Menu mMenu;

	/**
	 * onCreate - sets up the core activity members
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		GDAndroid.getInstance().activityInit(this);

		setContentView(R.layout.mainfragment);

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);

        mMenu = menu;
        updateConnectedApplicationButtons(ConnectedApplicationControl.getInstance().getCurrentState());

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.action_activate_application){

            if(ConnectedApplicationControl.getInstance().isConnectedApplicationActivationAllowed()) {
                //If ConnectedApplicationActivation is allowed then start it

                ConnectedApplicationState state = ConnectedApplicationControl.getInstance().getCurrentState();

                // Here we simply get the first connected application which is pending activation and activate that
                //If there were multiple then it would be possible to create a UI picker to show user which one to activate
                String address = state.getAppsToActivate().iterator().next();

                ConnectedApplicationControl.getInstance().startConnectedApplicationActivation(this,address);
            }

        } else if(item.getItemId() == R.id.action_manage_connected_apps) {

            //Start Activity to show connected application management
            Intent i = new Intent();
            i.setClass(this, ConnectedApplicationManagerActivity.class);
            startActivity(i);

        } else {

            if (m_fragment != null)
                m_fragment.onClick(item.getItemId());
        }
		return super.onOptionsItemSelected(item);
	}

    private void loadUIFragmentIfNeeded() {
        FragmentManager fragmentManager = getFragmentManager();

        Fragment fragment = fragmentManager.findFragmentByTag(FRAGMENT_TAG);
        if(fragment ==null) {

            //UI is not already loaded so we load it now
            m_fragment = new FileBrowserFragment();

            FragmentTransaction fragmentTransaction = fragmentManager
                    .beginTransaction();
            fragmentTransaction.replace(R.id.listFragmentSpace,
                    m_fragment, FRAGMENT_TAG);
            fragmentTransaction.commit();
        }
    }

    private void removeUIFragment() {

        FragmentManager fragmentManager = getFragmentManager();

        Fragment f = fragmentManager.findFragmentByTag(FRAGMENT_TAG);

        if (f != null) {

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(f);
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        //This specific Activity is only interested in Auth events when it is in the foreground
        AppGDStateControl.getInstance().addAppStateListener(this);

        if(AppGDStateControl.getInstance().getCurrentState() == AppGDStateControl.State.GD_Authorized)
        {
            //We are already authorized so we can show our UI
            loadUIFragmentIfNeeded();
        }

        ConnectedApplicationControl.getInstance().addConnectedAppStateListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        AppGDStateControl.getInstance().removeAppStateListener(this);
        ConnectedApplicationControl.getInstance().removeConnectedAppStateListener(this);

        /*
        We remove the Fragment here because otherwise the system remembers it was loaded and if it kills the process
        for memory reasons when it is restarted it will automatically attempt to add the Fragment when we would be in an
        non Authorized state
         */
        removeUIFragment();
    }

    @Override
	public void onClick(View v) {

		if(m_fragment != null){
			m_fragment.onClick(v.getId());
			switch (v.getId()) {
			case R.id.action_btn_container:
				updateBtns(R.id.action_btn_container, R.id.action_btn_sdcard);
				break;
			case R.id.action_btn_sdcard:
				updateBtns(R.id.action_btn_sdcard, R.id.action_btn_container);
				break;	
			}
		}
	}

	public void updateBtns(int enabledButton, int disabledButton) {
		((ToggleButton) findViewById(enabledButton)).setChecked(true);
		((ToggleButton) findViewById(disabledButton)).setChecked(false);
	}

    @Override
    public void onAppGDStateChanged(AppGDStateControl.State aNewState) {

        if(AppGDStateControl.getInstance().getCurrentState() == AppGDStateControl.State.GD_Authorized)
        {
            //We are already authorized so we can show our UI
            loadUIFragmentIfNeeded();
        } else if(AppGDStateControl.getInstance().getCurrentState() == AppGDStateControl.State.GD_NotAuthorized) {
            //We remove our Fragment because UI uses GD APIs
            removeUIFragment();
        }

    }

    private void updateConnectedApplicationButtons(ConnectedApplicationState aState){

        MenuItem promptActivate = mMenu.findItem(R.id.action_activate_application);
        MenuItem connectedApp = mMenu.findItem(R.id.action_application_connected);
        MenuItem activateDisallowed = mMenu.findItem(R.id.action_activate_application_disallowed);
        MenuItem manageConnectedApplications = mMenu.findItem(R.id.action_manage_connected_apps);

        DEBUG_LOG("updateConnectedApplicationButtons ConnectedApplicationState = " + aState.dumpConnectedApplicationState());

        //If No App Connected then we show neither icon
        if(aState.isNoAppConnected()){
            promptActivate.setVisible(false);
            connectedApp.setVisible(false);
            manageConnectedApplications.setVisible(false);
        } else {

            // It is possible to have multiple connected applications so could have both applications pending activation and connected

            if (aState.isAppPendingActivation()) {

                if (ConnectedApplicationControl.getInstance().isConnectedApplicationActivationAllowed()) {
                    promptActivate.setVisible(true);
                } else {
                    activateDisallowed.setVisible(true);
                }

            } else {
                promptActivate.setVisible(false);
                activateDisallowed.setVisible(false);
            }

            if (aState.isAppConnected()) {
                connectedApp.setVisible(true);
            }else {
                connectedApp.setVisible(false);
            }

            manageConnectedApplications.setVisible(true);

        }
    }

    /* @Override */
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(m_fragment != null) {
            // Fragment has its own request permission API, but since we remove FileBrowserFragment with onPause callback,
            // this fragment won't be attached again to parent Activity so we can't relay on that API.
            m_fragment.onPermissionResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onConnectedApplicationStateChanged(ConnectedApplicationState aState) {

        updateConnectedApplicationButtons(aState);

    }
}
