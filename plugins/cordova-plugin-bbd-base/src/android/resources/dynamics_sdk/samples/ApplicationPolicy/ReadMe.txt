### Application Policy ###

================================================================================
DESCRIPTION:
An example of how to use Good Dynamics Application Specific Policy APIs.
Update the policy on the Good Control server and the application will receive the updates.
If the 'Dashboard Tabs to Display' part of the policy is changed the application UI will be updated.

For more help of how to build and install this application, please see BUILD_NOTES.

For more examples visit the Good Developer Network:
https://begood.good.com/community/gdn

Support:
https://begood.good.com/community/gdn/support
================================================================================
(c) 2015 Good Technology Corporation. All rights reserved.

