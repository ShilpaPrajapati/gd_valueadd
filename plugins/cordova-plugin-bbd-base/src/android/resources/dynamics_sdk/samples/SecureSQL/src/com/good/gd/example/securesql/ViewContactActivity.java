/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.securesql;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.TextView;

import com.good.gd.Activity;
import com.good.gd.example.utils.ContactsDBUtils.Contact;
import com.good.gd.example.utils.DbContract;

/**
 * ViewContactActivity - views contact fields. Must be passed a contact ID to view.
 */
public class ViewContactActivity extends Activity {

    private long _contactRecId = -1;

    /**
     * onCreate - creates the viewer activity based on the passed contact id
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_contact);

        Intent i = getIntent();
        if (i != null) {
            _contactRecId = i.getLongExtra(DbContract.CONTACTS_FIELD_ID, -1);
        }
        if (savedInstanceState != null) {
            _contactRecId = savedInstanceState.getLong(DbContract.CONTACTS_FIELD_ID);
        }

        if (_contactRecId > 0) {
        	Contact c = null;
			Cursor cursor = getContentResolver().query(DbContract.CONTENT_URI, null, DbContract.CONTACTS_FIELD_ID + "=" + _contactRecId, null, null);
			cursor.moveToFirst();
	        if (!cursor.isAfterLast()) {
	            String firstName = cursor.getString(1);
	            String secondName = cursor.getString(2);
	            String phoneNumber = cursor.getString(3);
	            String notes = cursor.getString(4);
	            c = new Contact(firstName, secondName, phoneNumber, notes);
	        }
            ((TextView) findViewById(R.id.firstName)).setText(c.getFirstName());
            ((TextView) findViewById(R.id.secondName)).setText(c.getSecondName());
            ((TextView) findViewById(R.id.phoneNumber)).setText(c.getPhoneNumber());
            ((TextView) findViewById(R.id.notes)).setText(c.getNotes());
        }
    }

    /**
     * onSaveInstanceState - preserves the current id for a rotation
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(DbContract.CONTACTS_FIELD_ID, _contactRecId);
    }
}
