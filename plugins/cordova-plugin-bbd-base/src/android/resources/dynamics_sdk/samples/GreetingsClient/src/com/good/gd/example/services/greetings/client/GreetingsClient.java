/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.services.greetings.client;

import android.app.Activity;
import android.app.FragmentManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.good.gd.GDAndroid;
import com.good.gd.GDStateListener;
import com.good.gd.file.File;
import com.good.gd.file.FileOutputStream;
import com.good.gd.icc.GDICCForegroundOptions;
import com.good.gd.icc.GDServiceClient;
import com.good.gd.icc.GDServiceException;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

public class GreetingsClient extends Activity implements GDStateListener,
        OnClickListener {

    private static final String TAG = "GreetingsClient";

    private static final String _greetingServerId = "com.good.gd.example.services.greetings.server";

    // test files
    private volatile String _file1;
    private volatile String _file2;

    public static long _startTime;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG,
                "######################## Greetings Client onCreate ########################\n");
        super.onCreate(savedInstanceState);

        GDAndroid.getInstance().activityInit(this);

        setContentView(R.layout.main);
        showNotAuthorizedUI();

        GreetingsClientGDServiceListener.getInstance().setCurrentContext(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // Implementation of button actions
    private void _sendGreeting() {
        try {
            String customServiceMessage = "Hello from GreetingsClient";
            String requestID = GDServiceClient.sendTo(_greetingServerId,
                    "testService", "1.0.0", "testMethod", customServiceMessage,
                    null, GDICCForegroundOptions.PreferMeInForeground);
            Log.d(TAG, "sendGreeting requestID=" + requestID);
        } catch (GDServiceException e) {
            displayMessage("Error", e.getMessage());
        }
    }

    private void _sendGreetingWithAttachments() {
        try {
            String customServiceMessage = "Hello from GreetingsClient";

            String files[] = new String[2];
            files[0] = _file1;
            files[1] = _file2;

            String requestID = GDServiceClient.sendTo(_greetingServerId,
                    "testService", "1.0.0", "testMethod", customServiceMessage,
                    files, GDICCForegroundOptions.PreferMeInForeground);
            Log.d(TAG, "sendGreeting requestID=" + requestID);
        } catch (GDServiceException e) {
            displayMessage("Error", e.getMessage());
        }
    }

    private void _getAgeOfEmployeeWithName(String name) {
        try {
            String requestID = GDServiceClient.sendTo(_greetingServerId,
                    "search", "1.0.0", "age", name, null,
                    GDICCForegroundOptions.PreferMeInForeground);
            Log.d(TAG, "sendGreeting requestID=" + requestID);
        } catch (GDServiceException e) {
        	displayMessage("Error", e.getMessage());

        }
    }

    // UI
    public void showAuthorizedUI() {
        TextView t = (TextView) findViewById(R.id.authStatus);
        t.setText("Authorized");
        t.setTextColor(Color.rgb(0, 255, 0));
    }

    public void showNotAuthorizedUI() {
        TextView t = (TextView) findViewById(R.id.authStatus);
        t.setText("Not Authorized");
        t.setTextColor(Color.rgb(255, 0, 0));
    }

    public void setStatus(String status) {
        TextView t = (TextView) findViewById(R.id.status);
        t.setText(status);
    }

    public void displayMessage(String title, String message) {
        Log.d(TAG, "displayMessage title: " + title + " message:" + message
                + "\n");

        FragmentManager fm = getFragmentManager();
        GreetingsClientDialogFragment usermess = GreetingsClientDialogFragment.createInstance(title,message);
        usermess.show(fm, "usermessage");

    }

    // test method
    String test_createFile(String filename, int size) {
        Log.d(TAG, "+ createFile: " + filename + "\n");
        String path = filename;
        File file = new File(path);
        if (file.isDirectory()) {
            Log.d(TAG, "+ createFile deleting directory");
            file.delete();
        }
        String parent = file.getParent();
        if (parent != null) {
            File parentfile = new File(parent);
            boolean created = parentfile.mkdirs();
            if (!created) {
                Log.d(TAG, "+ createFile: couldn't create directories");
            } else {
                Log.d(TAG, "+ createFile: created directories");
            }
        }

        byte[] data = new byte[size];
        for (int i = 0; i < data.length; ++i) {
            data[i] = 2; // just fill the file with a random value
        }

        OutputStream out = null;
        try {
            Log.d(TAG, "+ createFile" + path + "\n");
            out = new BufferedOutputStream(new FileOutputStream(file));
            out.write(data);
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "- createFile - created\n");
        return path;
    }

    @Override
    public void onAuthorized() {

        Log.d(TAG, "onAuthorized( )");

        showAuthorizedUI();

        // create two test files
        if (_file1 == null || _file2 == null) {
            int fileSize = 10000;
            _file1 = test_createFile("sendFile1.txt", fileSize);
            _file2 = test_createFile(
                    "/\u6c49\u8bed/\u6f22\u8a9e/sendfile2\u65e5\u672c\u56fd.txt",
                    fileSize);
        }
    }

    @Override
    public void onLocked() {
        Log.d(TAG, "onLocked( )");
        showNotAuthorizedUI();
    }

    @Override
    public void onWiped() {
        Log.d(TAG, "onWiped( )");

    }

    @Override
    public void onUpdateConfig(Map<String, Object> settings) {
        Log.d(TAG, "onUpdateConfig( )");

    }

    @Override
    public void onUpdatePolicy(Map<String, Object> policyValues) {
        Log.d(TAG, "onUpdatePolicy( )");

    }

    @Override
    public void onUpdateServices() {
        Log.d(TAG, "onUpdateServices( )");

    }

    @Override
    public void onUpdateDataPlan() {
        Log.d(TAG, "onUpdateDataPlan( )");

    }

    @Override
    public void onUpdateEntitlements() {
        Log.d(TAG, "onUpdateEntitlements( )");
        
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_bring_to_front:
                actionBringToFront();
                break;
            case R.id.action_send_greetings:
                actionSendGreetings();
                break;
            case R.id.action_send_greeting_with_attachments:
                actionSendGreetingWithAttachments();
                break;
            case R.id.action_bob:
                actionBob();
                break;
            case R.id.action_xavier:
                actionXavier();
                break;
            case R.id.action_open_fingerprint_ui:
                actionOpenFingerprintUI();
                break;
            default:
                break;
        }

    }

    private void actionBringToFront() {
        Log.d(TAG, "+ bringToFront");
        try {
            GDServiceClient.bringToFront(_greetingServerId);
        } catch (GDServiceException e) {
            displayMessage("Error", e.getMessage());
        }
        Log.d(TAG, "- bringToFront");
    }

    private void actionSendGreetings() {
        Log.d(TAG, "+ sendGreeting");
        _startTime = System.currentTimeMillis();
        _sendGreeting();
        setStatus("Waiting...");
        Log.d(TAG, "- sendGreeting");
    }

    private void actionSendGreetingWithAttachments() {
        Log.d(TAG, "+ sendGreetingWithAttachments");
        _startTime = System.currentTimeMillis();
        _sendGreetingWithAttachments();
        setStatus("Waiting...");
        Log.d(TAG, "- sendGreetingWithAttachments");

    }

    private void actionXavier() {
        Log.d(TAG, "+ getXavierAge");
        _startTime = System.currentTimeMillis();
        _getAgeOfEmployeeWithName("xavier");
        setStatus("Waiting...");
        Log.d(TAG, "- getXavierAge");
    }

    private void actionBob() {
        Log.d(TAG, "+ getBobsAge");
        _startTime = System.currentTimeMillis();
        _getAgeOfEmployeeWithName("bob");
        setStatus("Waiting...");
        Log.d(TAG, "- getBobsAge");
    }

    private void actionOpenFingerprintUI() {
        GDAndroid.getInstance().openFingerprintSettingsUI();
    }
}
