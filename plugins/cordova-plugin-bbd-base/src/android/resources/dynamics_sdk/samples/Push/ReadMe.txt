### Push Channel ###

================================================================================
DESCRIPTION:
An example of how to use the Good Dynamics Push infrastructure. The connection can be controlled, channels created and messages sent in a loopback manner to the client.

For more examples visit the Good Developer Network:
https://begood.good.com/community/gdn

Support:
https://begood.good.com/community/gdn/support
================================================================================
(c) 2015 Good Technology Corporation. All rights reserved.