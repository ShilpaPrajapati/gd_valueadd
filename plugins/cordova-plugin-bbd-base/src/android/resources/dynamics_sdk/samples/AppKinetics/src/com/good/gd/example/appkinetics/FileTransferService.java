/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.appkinetics;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Vector;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.good.gd.GDAndroid;
import com.good.gd.GDServiceProvider;
import com.good.gd.GDServiceProviderType;

/**
 * Manages file transfer capable applications
 *
 */
public class FileTransferService {

	// Instance Variables -----------------------------------------------------
	private Vector<GDServiceProvider> appDetails = null;
	String appPackageName = null;
	
	// Constructors -----------------------------------------------------------
	public FileTransferService(Context context) {
		if (null == context) {
			throw new IllegalArgumentException("cannot handle null context!");
		}
		
        this.appDetails = GDAndroid.getInstance().getServiceProvidersFor(AppKineticsHelpers.SERVICENAME,
                                                                         AppKineticsHelpers.VERSION,
                                                                         GDServiceProviderType.GDSERVICEPROVIDERAPPLICATION);
		this.appPackageName = context.getPackageName();
		
	}

	// Public Methods -----------------------------------------------------
	/**
	 * getList - get list of file transfer services on this device
	 * 
	 * @return
	 */
	public List<GDServiceProvider> getList() {
		final List<GDServiceProvider> options = new ArrayList<GDServiceProvider>();
        this.appDetails = GDAndroid.getInstance().getServiceProvidersFor(AppKineticsHelpers.SERVICENAME,
                                                                         AppKineticsHelpers.VERSION,
                                                                         GDServiceProviderType.GDSERVICEPROVIDERAPPLICATION);
        
		for (GDServiceProvider detail : this.appDetails) {
			final String servicePackage = detail.getAddress();

			if ( otherApplication(servicePackage)) 
			{ 
				final String serviceName = detail.getName();
				if (!TextUtils.isEmpty(serviceName))
				{
					options.add(detail); 
					if(detail.getIcon() == null)
					{
						Log.d(AppKineticsHelpers.LOGTAG,
				                "Icon for "+detail.getName()+" is NULL");
					}
					else
					{
						Log.d(AppKineticsHelpers.LOGTAG,
				                "Icon for "+detail.getName()+" is not NULL");
					}
				}
			}
		}
		return options;
	}

	/**
	 * addressLookup - return the app detail address based on the specified app
	 * name
	 * 
	 * @param name
	 * @return
	 */
	public String addressLookup(String name) {
		if (TextUtils.isEmpty(name) || (null == this.appDetails)) {
			throw new NoSuchElementException(name);
		}

		for (GDServiceProvider detail : this.appDetails) {
			if (name.equals(detail.getName())) {
				return detail.getAddress();
			}
		}
		throw new NoSuchElementException(name);
	}

	// Private Methods --------------------------------------------------------
	/**
	 * otherApplication - checks if string refers to self (this package)
	 * 
	 * @param packageName
	 * @return
	 */
	private boolean otherApplication(final String packageName) {
		return ! this.appPackageName.equalsIgnoreCase(packageName);
	}
}
