1 Inter-Application Communication
1.1 Feature Overview
The inter-application communication feature enables secure exchange of data between two Good Dynamics applications running on the same device.
Inter-application communication is an expansion of the application data exchange capability that is available in the Secure Documents API. The Secure Documents API enables exchange of individual files. Inter-application communication enables multiple files to be sent in a single exchange, and allows for a command with parameters to be specified.
The Secure Documents API is a simple file transfer capability. Inter-application communication is a rich and secure collaboration protocol.
See the following document on GDN for an introduction to the Secure Documents API:
https://begood.good.com/docs/DOC-1239
See also the container management diagram under Authentication Delegation, below.
1.2 Feature Description
Inter-application communication provides a data exchange mechanism that is similar to web services. The feature is based on a proprietary protocol, the Good Inter-Container Communication (ICC) protocol. ICC was created as part of the development of this feature by Good Engineering.
Under ICC, two applications on the same device can exchange data as follows (ICC terms are in italics):
1. The client application sends a request to a method in the server application. The request can include a number of parameters, and a number of attachments.
2. The server application receives the request, and possibly executes some processing.
3. The server application can then either:
send a normal response back to the client application, or send an error response back to the client application, or send no response.
The terms called out in the above are discussed in more detail in the following sections.
See also the Example of Services Framework and Inter-Application Communication in the appendix.

