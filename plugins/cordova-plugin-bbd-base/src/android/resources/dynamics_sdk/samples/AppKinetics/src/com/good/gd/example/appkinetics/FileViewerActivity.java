/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.appkinetics;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;

import com.good.gd.GDAndroid;
import com.good.gd.GDServiceProvider;

/**
 * File viewer screen
 */
public class FileViewerActivity extends Activity implements OnClickListener {

    // Constants --------------------------------------------------------------
    public static final String FILE_VIEWER_PATH = "path";

    // Static Variables -------------------------------------------------------
    private static final Map<String, String> mimeTypes;

    private static final int ITEM_MAIN_ACTION_BAR_CONTAINER = 0;

    // Static Initializer -----------------------------------------------------
    static {
        mimeTypes = new HashMap<String, String>();
        mimeTypes.put(".txt", "text/plain");
        mimeTypes.put(".xml", "text/xml");
        mimeTypes.put(".html", "text/html");
        mimeTypes.put(".htm", "text/html");
        mimeTypes.put(".jpg", "image/jpeg");
        mimeTypes.put(".jpeg", "image/jpeg");
        mimeTypes.put(".png", "image/png");
    }

    // Instance Variables -----------------------------------------------------
    private String _filePath;

    // Public Methods ---------------------------------------------------------

    /**
     * onCreate - takes a path from the caller to get the file from
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(AppKineticsHelpers.LOGTAG, "FileViewerActivity.onCreate()\n");
        super.onCreate(savedInstanceState);

        GDAndroid.getInstance().activityInit(this);

        setContentView(R.layout.viewer);

        Intent i = getIntent();
        if (i != null) {
            _filePath = i.getStringExtra(FILE_VIEWER_PATH);
        }
    }

    /**
     * onResume - sets up and loads data into the webview
     */
    public void onResume() {
        Log.d(AppKineticsHelpers.LOGTAG, "FileViewerActivity.onResume()\n");
        super.onResume();

        WebView wv = (WebView) findViewById(R.id.webview);

        String fileExt = _filePath.substring(_filePath.lastIndexOf('.'));
        fileExt = fileExt.toLowerCase();
        if (mimeTypes.containsKey(fileExt)) {
            String mimeType = mimeTypes.get(fileExt);

            byte b[] = AppKineticsModel.getInstance().getFileData(_filePath);
            try {
                if (b != null && b.length > 0) {
                    if (fileExt.equals(".jpg") || fileExt.equals(".jpeg")
                            || fileExt.equals(".png")) {
                        String b64Data = Base64.encodeToString(b,
                                Base64.DEFAULT);
                        wv.loadData(b64Data, mimeType, "base64");
                    } else
                        wv.loadData(new String(b, "UTF-8"), mimeType, "UTF-8");
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            // Unsupported file type
            String html = "<h1>Unsupported File Type<h1><p/><h2>%1$s</h2><p/><h4>Use external apps to view this file</h4>";
            String html2 = String.format(html, _filePath);
            wv.loadData(html2, "text/html", "utf-8");
        }
        
        if (AppKineticsModel.getInstance().isAuthorized()
				&& ! AppKineticsModel.getInstance().getPendingFileList()
						.isEmpty()) {
			Intent i = new Intent(getApplicationContext(),
					AppKinetics.class);
			startActivity(i);
		}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.getItem(ITEM_MAIN_ACTION_BAR_CONTAINER).getActionView()
                .findViewById(R.id.right_seperator).setVisibility(View.GONE);
        menu.getItem(ITEM_MAIN_ACTION_BAR_CONTAINER).getActionView()
                .findViewById(R.id.action_reset).setVisibility(View.GONE);
        return true;
    }

    public void showSendToDialog() {
        final FileTransferService fts = new FileTransferService(this);
        final List<GDServiceProvider> svcLst = fts.getList();
        // Use the Builder class for convenient dialog construction
        
        
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.send_to)
                .setAdapter(
                        new IconAndTextListAdapter(this, 
                        		svcLst),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                            	GDServiceProvider svc = svcLst.get(which);
                                String svcAddrs = fts.addressLookup((String) svc.getName());
                                List<String> files = new ArrayList<String>();
                                files.add(_filePath);
                                AppKineticsModel.getInstance().sendFiles(
                                        svcAddrs, files);
                            }
                        }
                )
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        }
                ).setCancelable(true);
		AlertDialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_send_to:
                showSendToDialog();
                break;
            case R.id.action_delete:
                AppKineticsModel.getInstance().deleteFile(_filePath);
                finish();
                break;
        }

    }
}