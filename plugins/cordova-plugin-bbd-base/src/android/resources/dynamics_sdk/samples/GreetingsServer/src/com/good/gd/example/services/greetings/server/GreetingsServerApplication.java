package com.good.gd.example.services.greetings.server;

import com.good.gd.icc.GDService;
import com.good.gd.icc.GDServiceException;
import com.good.gd.icc.GDServiceListener;

import android.app.Application;
import android.util.Log;


public class GreetingsServerApplication extends Application {

	static final String TAG = GreetingsServerApplication.class.getSimpleName();
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		GDServiceListener serv = GreetingsServerGDServiceListener.getInstance();
		
		Log.e(TAG , "GreetingsServerApplication::onCreate() service Listener = " + serv + "\n");
		
		if(serv!=null){
    		//Set the Service Listener to get requests from clients
    		try {
    			GDService.setServiceListener(serv);
    		} catch (GDServiceException e) {
    			Log.e(TAG , "GreetingsServerApplication::onCreate()  Error Setting GDServiceListener --" + e.getMessage()  + "\n");
    		}
    	}
	}

	
	
	
}
