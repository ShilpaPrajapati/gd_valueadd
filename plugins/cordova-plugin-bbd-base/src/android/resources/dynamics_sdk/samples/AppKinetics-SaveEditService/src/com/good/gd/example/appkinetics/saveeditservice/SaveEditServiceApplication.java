/**
 * SaveEditServiceApplication.java
 */
package com.good.gd.example.appkinetics.saveeditservice;

import android.app.Application;
import android.util.Log;

import com.good.gd.icc.GDService;
import com.good.gd.icc.GDServiceException;
import com.good.gd.icc.GDServiceListener;

public class SaveEditServiceApplication extends Application {

    private static final String TAG = SaveEditServiceApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();

        final GDServiceListener serviceListener = GDSaveEditServiceListener.getInstance();

        try {
            GDService.setServiceListener(serviceListener);
        } catch (final GDServiceException exception) {
            Log.e(TAG, "SaveEditServiceApplication::onCreate() " +
                    "Error Setting GDServiceListener -- " + exception.getMessage() + "\n");
        }
    }
}