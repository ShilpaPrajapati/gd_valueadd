/*
 * Copyright (c) Good Technology Corporation 2016. All rights reserved.
 */

package com.good.automated_test_support;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.good.gd.GDStateAction;

import java.util.Map;

/*
 GDSDKStateReceiver is a helper class for automated tests to receive GD SDK states broadcasts from
 GDLocalBroadcastManager. The GD SDK states it's interested in are authorized, locked, and wiped.
 However, more state actions (e.g., policy updated, config updated, service updated, or entitlements
 updated)  can be registered to receive broadcast notifications. See GDStateAction
 class for more details.

 This is important because as well as checking certain UI screens are dismissed (say in case of GD
 login) it is also important the GD has provided the authorized broadcast event before app code (or 
 other test code) runs.
 */
public class GDSDKStateReceiver extends BroadcastReceiver {
    private static final String TAG = "GD_TEST_BASE";
    private static GDSDKStateReceiver instance = null;
    private static Object MONITOR = new Object();
    private boolean mIsAuthorized = false;
    private boolean mIsReceivered = false;

    public static synchronized GDSDKStateReceiver getInstance() {
        if (instance == null) {
            instance = new GDSDKStateReceiver();
        }
        return instance;
    }

    private GDSDKStateReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(GDStateAction.GD_STATE_AUTHORIZED_ACTION);
        intentFilter.addAction(GDStateAction.GD_STATE_LOCKED_ACTION);
        intentFilter.addAction(GDStateAction.GD_STATE_WIPED_ACTION);

        Log.d(TAG, "Register GDStateReceiver. ");
        GDAutomatedTestSupport.registerGDStateReceiver(this, intentFilter);
    }

    public boolean checkAuthorized() {
        if (mIsAuthorized || mIsReceivered) {
            Log.d(TAG, "Unregister GDStateReceiver. ");
            GDAutomatedTestSupport.unregisterGDStateReceiver(this);
        }
        return mIsAuthorized;
    }

    public void waitForAuthorizedChange(int aTimeWaitMilliseconds) {

        synchronized (MONITOR) {
            try {
                MONITOR.wait(aTimeWaitMilliseconds);
            } catch (InterruptedException e) {

            }
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d(TAG, "GDSDKStateReceiver callback with action: " + action);
        mIsReceivered = true;

        if (action.equals(GDStateAction.GD_STATE_AUTHORIZED_ACTION)) {
            onAuthorized();
        } else if (action.equals(GDStateAction.GD_STATE_LOCKED_ACTION)) {
            onLocked();
        } else if (action.equals(GDStateAction.GD_STATE_WIPED_ACTION)) {
            onWiped();
        }
    }

    // App is authorized
    public void onAuthorized() {
        mIsAuthorized = true;

        synchronized (MONITOR) {
            MONITOR.notify();
        }
    }

    // App is locked
    public void onLocked() {
        mIsAuthorized = false;

        synchronized (MONITOR) {
            MONITOR.notify();
        }
    }

    // App is wiped
    public void onWiped() {
        mIsAuthorized = false;

        synchronized (MONITOR) {
            MONITOR.notify();
        }
    }
}
