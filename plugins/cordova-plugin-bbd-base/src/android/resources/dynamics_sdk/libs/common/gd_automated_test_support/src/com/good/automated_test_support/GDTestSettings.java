/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2016 Good Technology Corporation. All rights reserved.
 */
package com.good.automated_test_support;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.res.AssetManager;


public class GDTestSettings {


	private static final String GD_TEST_PROVISION_EMAIL = "GD_TEST_PROVISION_EMAIL";
	private static final String GD_TEST_PROVISION_ACCESS_KEY = "GD_TEST_PROVISION_ACCESS_KEY";
	private static final String GD_TEST_PROVISION_PASSWORD = "GD_TEST_PROVISION_PASSWORD";

	private static final String GD_TESTSETTINGS_FILENAME = "com.good.gd.test.json";

	private static GDTestSettings instance = null;

	private Map<String,JSONObject> gdTestSettingsJsonObjects = null;
	private String defaultPackageName = null;

	public static synchronized GDTestSettings getInstance() {
		if (instance == null) {
			instance = new GDTestSettings();
		}
		return instance;
	}

	private GDTestSettings() {
	}

	public void initialize(Context aTestctx, String defaultPackageName) {
		this.defaultPackageName = defaultPackageName;

		try {
			gdTestSettingsJsonObjects = new HashMap<>();

			AssetManager assetManager = aTestctx.getAssets();
			for (String file: assetManager.list("")) {
                if (file.endsWith(".json")) {
                    String settingsJson = getSettingsJson(assetManager, file);
                    if (settingsJson != null) {
                        try {
                            JSONObject gdTestSettingsJsonObject = new JSONObject(settingsJson);
                            if (validateSettingsFile(gdTestSettingsJsonObject)) {
                                gdTestSettingsJsonObjects.put(file.equals(GD_TESTSETTINGS_FILENAME) ? defaultPackageName : file.replace(".json", ""), gdTestSettingsJsonObject);
                            }
                        } catch (JSONException e) {
                            Assert.fail("GD::TestSettings -Error parsing data "
                                    + e.toString());
                        }
                    }
                }
            }
		} catch (IOException e) {
			Assert.fail("GD::TestSettings -Error parsing data "
					+ e.toString());
		}
	}

	public String getAppProvisionEmail() {
		return getAppProvisionEmail(defaultPackageName);
	}

	public String getAppProvisionEmail(String packageName) {
		return getStringAttribute(packageName, GD_TEST_PROVISION_EMAIL);
	}

	public String getAppProvisionAccessKey() {
		return getAppProvisionAccessKey(defaultPackageName);
	}

	public String getAppProvisionAccessKey(String packageName) {
		return getStringAttribute(packageName, GD_TEST_PROVISION_ACCESS_KEY);
	}

	public String getAppProvisionPassword() {
		return getAppProvisionPassword(defaultPackageName);
	}

	public String getAppProvisionPassword(String packageName) {
		return getStringAttribute(packageName, GD_TEST_PROVISION_PASSWORD);
	}

	private boolean validateSettingsFile(JSONObject object) {
		boolean result = false;
		try {
			result = object.getString(GD_TEST_PROVISION_EMAIL) != null && object.getString(GD_TEST_PROVISION_EMAIL).length() > 0 &&
					 object.getString(GD_TEST_PROVISION_PASSWORD) != null && object.getString(GD_TEST_PROVISION_PASSWORD).length() > 0 &&
					 object.getString(GD_TEST_PROVISION_ACCESS_KEY) != null && object.getString(GD_TEST_PROVISION_ACCESS_KEY).length() > 0;
		}catch (JSONException e){

		} finally {
			return result;
		}
	}

	private String getStringAttribute(String packageName, String key) {
		String result = null;

		if (gdTestSettingsJsonObjects != null) {
			try {
				result = gdTestSettingsJsonObjects.get(packageName).getString(key);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	// read the assets/settings.json file from the APK bundle; return its
	// contents as a String
	private static String getSettingsJson(AssetManager assetManager, String settingsFile) {
		InputStream is = null;
		try {
			is = assetManager.open(settingsFile);
			if (is == null) {
				throw new Exception("No such file");
			}
			return new String(readBytes(is), "UTF-8");
		} catch (Exception e) {
			Assert.fail("GD::TestSettings - Could not read " + settingsFile
					+ "[" + e.getMessage() + "]\n");
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
				}
			}
		}
		return null;
	}

	private static byte[] readBytes(InputStream is) throws IOException {
		byte[] buf = new byte[1024];
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		int bytesRead = 0;
		while (-1 != (bytesRead = is.read(buf))) {
			bout.write(buf, 0, bytesRead);
		}

		return bout.toByteArray();
	}

}
