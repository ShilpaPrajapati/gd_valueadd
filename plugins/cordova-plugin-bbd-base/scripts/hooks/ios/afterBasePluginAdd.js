#!/usr/bin/env node

/*
 * (c) 2016 BlackBerry Limited. All rights reserved.
 */

require('shelljs/global');

module.exports = function(context) {
	var fs = context.requireCordovaModule('fs');
	var path = context.requireCordovaModule('path');
	
    var platformRoot = path.join(context.opts.projectRoot, 'platforms', 'ios');
	var platformFiles = fs.readdirSync(platformRoot);

	var srcDirName = null;
	var xcodeProjectName = null;

	for (var valueIndex in platformFiles) {
		var value = platformFiles[valueIndex];
		if (value.indexOf('.xcodeproj') > -1) {
			xcodeProjectName = value;
			var strEndPosition = value.indexOf('.');
			srcDirName = value.substring(0, strEndPosition);
			break;
		}
	}

	if (srcDirName == null) {
		throw new Error("srcDirName == null");
	}
	
	//We need to read settings from default.xcconfig file
	//We need to set those settings to platforms/ios/cordova/build.xcconfig file
	//Those settings will be available in Build Settings in Xcode
	//In Cordova 6.x build configuration files (*.xcconfig) are selected in Cordova Xcode project
	//In Cordova 5.x we will notify user to do it manually manually - select Cordova's build.xcconfig as build configyration file

	var defaultXcconfigPath = path.join(context.opts.projectRoot, 'plugins', 'cordova-plugin-bbd-base', 'src', 'ios', 'default.xcconfig');
	chmod(660, defaultXcconfigPath);
	if (fs.existsSync(defaultXcconfigPath)) {
		var defaultXcconfigContent = fs.readFileSync(defaultXcconfigPath, { encoding: 'utf8' });
		var FIPS_PACKAGE_PROP = 'FIPS_PACKAGE';
		
		if (defaultXcconfigContent.indexOf(FIPS_PACKAGE_PROP) > -1) {
			var commentPrefix = '\n\n\/\/ Build configuration for GD iOS SDK. Please do not remove it!!!\n';
			var defaultXcconfigContentNoComments = commentPrefix + defaultXcconfigContent.substring(defaultXcconfigContent.indexOf(FIPS_PACKAGE_PROP), defaultXcconfigContent.length);
			var cordovaBuildScconfigPath = path.join(context.opts.projectRoot, 'platforms', 'ios', 'cordova', 'build.xcconfig');

			if (fs.existsSync(cordovaBuildScconfigPath)) {
				var cordovaBuildXcconfigContent = fs.readFileSync(cordovaBuildScconfigPath, { encoding: 'utf8' });
				
				if (cordovaBuildXcconfigContent.indexOf(FIPS_PACKAGE_PROP) < 0) {
					fs.appendFileSync(cordovaBuildScconfigPath, defaultXcconfigContentNoComments);
				}

			} else {
				throw new Error("Wrong path to build.xcconfig file.");
			}

		} else {
			throw new Error(FIPS_PACKAGE_PROP + " property does not exist in default.xcconfig file.");
		}

	} else {
		throw new Error("Wrong path to default.xcconfig file.");
	}

	// TODO: it is better to swizzle UIApplicationMain method in main.m file
	// we should remove this section after swizzling is done
	var pluginPath = context.opts.plugin.dir;
	cd(path.join(platformRoot, srcDirName));
	chmod(660, 'main.m');
	// temporary copy original Cordova's main.m file to be able to put it back when Base plugin is removed
	if (!fs.existsSync(path.join(pluginPath, 'src', 'ios','temp', 'main.m'))) {
		mkdir(path.join(pluginPath, 'src', 'ios','temp'));
		chmod(770, path.join(pluginPath, 'src', 'ios','temp'));
		cp(path.join(platformRoot, srcDirName, 'main.m'), path.join(pluginPath, 'src', 'ios', 'temp', 'main.m'));
	}
	// replacing Cordova's main.m file with our version in order to do initialization of GD
	rm(path.join(platformRoot, srcDirName, 'main.m'));
	cp(path.join(pluginPath, 'src', 'ios', 'main.m'), path.join(platformRoot, srcDirName, 'main.m'));

	// Manage plist file
	var plist = require('plist');
	var CordovaUtil = context.requireCordovaModule("cordova-lib/src/cordova/util");
	var ConfigParser = require('cordova-common').ConfigParser;
    var projectRoot = CordovaUtil.isCordova();
    var xml = CordovaUtil.projectConfig(projectRoot);
    var cfg = new ConfigParser(xml);
	var packageName = cfg.packageName();
	var plistFilePath = path.join(platformRoot, srcDirName, srcDirName + "-Info.plist" );
	var plistData = fs.readFileSync(plistFilePath, 'utf8');
	
	// 'plist' module removes empty string tags '<string></string>' or '<string/>' 
	// for optimization and plist becomes invalid after calling build() method.
	// To hack this behaviour we can just replace these empty tags with unique occupier string like '<string>s6metpq4mjomd0f3whfr</string>'.
	// We will randomly generate this occupier string with 20 characters of size
	// It will not affect anything, we will revert our changes after building plist file from JS object below.
	var uniqueOccupier = generateUniqueOccupierFor(plistData);
	plistData = plistData.replace(/<string\/>/g, '<string><\/string>');
	plistData = plistData.replace(/<string><\/string>/g, '<string>' + uniqueOccupier + '<\/string>');
	// parse plist to JS object
	var plistObj = plist.parse(plistData);

	if(!plistObj.GDApplicationID || plistObj.GDApplicationID.length == 0){
		if (plistObj.CFBundleURLTypes === undefined) {
			plistObj.CFBundleURLTypes = [
				{
					CFBundleURLSchemes : []
				}
			];
		}

		plistObj.CFBundleURLTypes[0].CFBundleURLName = packageName;
		plistObj.CFBundleURLTypes[0].CFBundleURLSchemes.push(packageName + '.sc2.1.0.0.0');
		plistObj.CFBundleURLTypes[0].CFBundleURLSchemes.push(packageName + '.sc2');
		plistObj.CFBundleURLTypes[0].CFBundleURLSchemes.push('com.good.gd.discovery');
		plistObj.GDApplicationID = plistObj.CFBundleIdentifier;
		plistObj.GDApplicationVersion = '1.0.0.0';
		plistObj.GDConsoleLogger = 'GDFilterDetailed';
		plistObj.UILaunchStoryboardName = 'LaunchScreen';

		// Removing NSMainNibFile from *.plist that does not exist in Cordova app and causes runtime crash
		if (plistObj.NSMainNibFile || plistObj.NSMainNibFile === null) {
			delete plistObj.NSMainNibFile;
		}
		if (plistObj["NSMainNibFile~ipad"] || plistObj["NSMainNibFile~ipad"] === null) {
			delete plistObj["NSMainNibFile~ipad"];
		}

		var outPlist = plist.build(plistObj);
		// reverting our changes and replacing '<string> </string>' on '<string></string>'
		var oldValue = '<string>' + uniqueOccupier + '<\/string>';
		var newValue = '<string><\/string>';
		outPlist = outPlist.replace(RegExp(oldValue, "g"), newValue);
		fs.writeFileSync(plistFilePath, outPlist, { encoding: 'utf8' });
	}

	// Copy our hook to application's hooks folder
	// It should execute 'after_plugin_rm' and should clear Cordova application after all BBD Cordova plugins are removed from the application
	// This hook will be self removed
	cp(path.join(pluginPath, 'scripts', 'hooks', 'ios', 'afterBasePluginRemove.sh'), path.join(context.opts.projectRoot, 'hooks', 'afterBasePluginRemove.sh'));
	chmod(770, path.join(context.opts.projectRoot, 'hooks', 'afterBasePluginRemove.sh'));
	// Make hook abvailable in main config.xml file of the application
	var configXmlPath = path.join(context.opts.projectRoot, 'config.xml');
	if (fs.existsSync(configXmlPath)) {
		var configXmlContent = fs.readFileSync(configXmlPath, { encoding: 'utf8' });
		if (configXmlContent.indexOf('<platform name="ios">') >= 0) {
			if (configXmlContent.indexOf('afterBasePluginRemove.sh') < 0) {
				configXmlContent = configXmlContent.replace('<platform name="ios">', '<platform name="ios">\n\t\t<hook type="after_plugin_rm" src="hooks/afterBasePluginRemove.sh" />');
				fs.writeFileSync(configXmlPath, configXmlContent, 'utf8');
			}
		}
	}

	function generateUniqueOccupierFor (text) {
		var uniqueStr = '';

		uniqueStr = Math.random().toString(36).substring(7);

		if (text.indexOf(uniqueStr) < 0) {
			return uniqueStr;
		} else {
			generateUniqueOccupierFor(text);
		}		
	}

	console.log('\x1b[32m%s\x1b[0m','Plugin cordova-plugin-bbd-base was successfully added.');
};