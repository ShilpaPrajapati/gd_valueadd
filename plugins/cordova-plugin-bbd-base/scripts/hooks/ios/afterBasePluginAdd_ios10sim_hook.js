#!/usr/bin/env node

/*
 * (c) 2016 BlackBerry Limited. All rights reserved.
 */

require('shelljs/global');

// This hook is to fix issue with iOS 10 Simulator
// https://developer.apple.com/library/content/releasenotes/DeveloperTools/RN-Xcode/Introduction.html
// This had to be fiexed in Xcode 8.2 but it wasn't
// We found following workaround:
// 1. add Entitlements.plist file
// 1.1. add property “application-identifier” with String type
// 1.2. set app identifier as property value
// 2. edit default.xcconfig file by adding next settings:
// ENTITLEMENTS_REQUIRED=YES
// CODE_SIGN_ENTITLEMENTS[sdk=iphonesimulator10.*]=$(SRCROOT)/(Path to Entitlements.plist file)
// This hook implements the steps above 

// This hook will be removed after issue is fixed

module.exports = function(context) {
	var fs = context.requireCordovaModule('fs');
	var path = context.requireCordovaModule('path');
	
    var platformRoot = path.join(context.opts.projectRoot, 'platforms', 'ios');
	var platformFiles = fs.readdirSync(platformRoot);
	

	var srcDirName = null;
	var xcodeProjectName = null;

	for (var valueIndex in platformFiles) {
		var value = platformFiles[valueIndex];
		if (value.indexOf('.xcodeproj') > -1) {
			xcodeProjectName = value;
			var strEndPosition = value.indexOf('.');
			srcDirName = value.substring(0, strEndPosition);
			break;
		}
	}

	if (srcDirName == null) {
		throw new Error("srcDirName == null");
	}

	// Here we correct the path to Entitlements.plist in default.xcconfig
	// Implement 1.1.
	var defaultXcconfigPath = path.join(context.opts.projectRoot, 'plugins', 'cordova-plugin-bbd-base', 'src', 'ios', 'default.xcconfig');
	chmod(660, defaultXcconfigPath);
	if (fs.existsSync(defaultXcconfigPath)) {
		var defaultXcconfigContent = fs.readFileSync(defaultXcconfigPath, { encoding: 'utf8' });
		
		defaultXcconfigContent = defaultXcconfigContent.replace('<appName>', srcDirName);
		fs.writeFileSync(defaultXcconfigPath, defaultXcconfigContent, { encoding: 'utf8' });
	}

	// Manage Entitlements.plist file
	// Implement 1.2.
	var plist = require('plist');
	var CordovaUtil = context.requireCordovaModule("cordova-lib/src/cordova/util");
	var ConfigParser = require('cordova-common').ConfigParser;
    var projectRoot = CordovaUtil.isCordova();
    var xml = CordovaUtil.projectConfig(projectRoot);
    var cfg = new ConfigParser(xml);
	var packageName = cfg.packageName();
	var plistFilePath = path.join(platformRoot, srcDirName, 'Resources', 'Entitlements.plist' );
	var plistObj = plist.parse(fs.readFileSync(plistFilePath, 'utf8'));

	chmod(660, plistFilePath);
	if (!plistObj['application-identifier']) {
		plistObj['application-identifier'] = packageName;
	}

	var outPlist = plist.build(plistObj);
	fs.writeFileSync(plistFilePath, outPlist, { encoding: 'utf8' });
}