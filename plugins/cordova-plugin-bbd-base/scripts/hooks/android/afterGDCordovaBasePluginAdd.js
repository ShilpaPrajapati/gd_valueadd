#!/usr/bin/env node

/*
 * (c) 2016 BlackBerry Limited. All rights reserved.
 */

var ncp = require('ncp').ncp;
var os = require('os');
require('shelljs/global');

module.exports = function(context) {

	/*read value with a given key from "key=value
					   					key2=value2" (properties file format)
	 string */
	var readProperty = function(data,key){
		var startOfValuePos = data.indexOf(key+'=') + key.length + 1;
		var endOfValuePos = data.indexOf('\n', startOfValuePos);
		
		return data.substring(startOfValuePos, endOfValuePos);
	}

	var fs = context.requireCordovaModule('fs'),
		path = context.requireCordovaModule('path');

    var platformRoot = path.join(context.opts.projectRoot, 'platforms','android');
    var assets = path.join(context.opts.projectRoot, 'platforms','android','assets');

    var manifestFile = path.join(platformRoot, 'AndroidManifest.xml');
    var settingsJson = path.join(assets, 'settings.json');
    var settingsGradle = path.join(platformRoot, 'settings.gradle');
    var gradleProperties = path.join(platformRoot, 'gradle.properties');
    var projectProperties = path.join(platformRoot, 'project.properties');

    // add GD Android SDK (gd, gd_backup_support) as dependency to the project
    // 1. copy 'gd' and 'gd_backup_support' to the project
    // 2. add 'gd' and 'gd_backup_support' to settings.gradle

    // get path to 'sdk' folder from gradle.properties
	// step 1.
	var gradlePropertiesData = fs.readFileSync(gradleProperties, 'utf8');
	var gdSdkPath = readProperty(gradlePropertiesData,'pathToGdAndroidSdk').replace(/\\ /g," ");
    var handheldPath = path.join(gdSdkPath,'libs','handheld','gd');
    var gdBackupSupportPath = path.join(gdSdkPath,'libs','handheld','gd_backup_support');

    // copy 'gd' module into project
    if (!fs.existsSync(handheldPath) || !fs.existsSync(gdBackupSupportPath)) {
    	throw new Error("Cannot find BlackBerry Dynamics SDK for Android. \"pathToGdAndroidSdk\" in gradle.properties is incorrect, place the path to it in cordova-plugin-bbd-base/scripts/gradle/gradle.properties and reinstall the plugin.");
    }
	ncp(handheldPath, path.join(platformRoot,'gd'), function (err) {
		if (err) {
			throw new Error(err);
		} else {
			var gdBuildGradle = path.join(platformRoot, 'gd','build.gradle');
			chmod(770, gdBuildGradle);
		}
	});

	// copy 'gd_backup_support' module into project
    ncp(gdBackupSupportPath, path.join(platformRoot,'gd_backup_support'), function (err) {
		if (err) {
			throw new Error(err);
		} else {
			var gdBackupSupportBuildGradle = path.join(platformRoot,'gd_backup_support', 'build.gradle');
			chmod(770, gdBackupSupportBuildGradle);
		}
	});

    if (fs.existsSync(settingsGradle)) {
    	try {
    		settingsGradleData = fs.readFileSync(settingsGradle, 'utf8');
    		// step 2.
    		if (settingsGradleData.indexOf('gd') == -1) {
    			var addGdToDeps = settingsGradleData.replace(/include ":CordovaLib"/g, ['include ":CordovaLib"','include ":gd"','include ":gd_backup_support"'].join(os.EOL));
    			fs.writeFileSync(settingsGradle, addGdToDeps, 'utf8');
    		}
    	} catch (err) {
    		throw new Error('An error occurred during updating settings.gradle: ' + err);
    	}
    } else {
    	throw new Error('Unable to find settings.gradle');
    }

    //set dependencies via project.properties if they doesn't exist
    if (fs.existsSync(projectProperties)) {
    	var findAllUniq = function(data, r) {
			var s = {};
			var m;
			while ((m = r.exec(data))) {
			    s[m[1]] = 1;
			}
			return Object.keys(s);
		}
		try {
			var projectPropertiesData = fs.readFileSync(projectProperties, 'utf8');
			//count other library references
			var subProjects = findAllUniq(projectPropertiesData, /^\s*android\.library\.reference\.\d+=(.*)(?:\s|$)/mg);

			// set library index
			var libIndex = subProjects.length + 1;

			if(projectPropertiesData.indexOf("=gd") == -1){
				fs.appendFile(projectProperties, os.EOL + 'android.library.reference.' + libIndex + '=gd', 'utf8');
				libIndex++;
			}
			
			if(projectPropertiesData.indexOf("=gd_backup_support") == -1){
				fs.appendFile(projectProperties, os.EOL + 'android.library.reference.' + libIndex + '=gd_backup_support', 'utf8');
			}

		} catch (err) {
			throw new Error('An error occurred during updating project.properties: ' + err);
		}

    } else {
    	throw new Error('Unable to find project.properties');
    }

    // configuring AndroidManifest.xml, settings.json, MainActivity.java
    if (fs.existsSync(manifestFile)) {
    	try {
    		var manifestFileData = fs.readFileSync(manifestFile, 'utf8');
    		var packageName = manifestFileData.substring(manifestFileData.indexOf('package="') + 9, manifestFileData.indexOf('xmlns:android') - 2);
			var appClass = 'com.good.gd.cordova.core.GDCordovaApp';
			var alwaysRetainTaskState = 'android:alwaysRetainTaskState';
			// changes to <application> and <activity> tags in AndroidManifest.xml
			if (manifestFileData.indexOf(appClass) == -1) {
				var applicationName = manifestFileData.replace(/<application/g, '<application android:name="' + appClass + '"');
				var applicationIcon = applicationName.replace('@drawable/icon', '@drawable/com_bbd_default_logo');
				// remove supportsRtl attribute and add fullBackupContent attribute
				var noSupportRtl = applicationIcon.replace(/ android:supportsRtl="true"/g, ' android:fullBackupContent="@xml/gd_backup_scheme"');
				fs.writeFileSync(manifestFile, noSupportRtl, 'utf8');
				if (manifestFileData.indexOf(alwaysRetainTaskState) == -1) {
				    var activityAlwayRetainTaskState = noSupportRtl.replace(/<activity/g, '<activity android:alwaysRetainTaskState="true"');
				    fs.writeFileSync(manifestFile, activityAlwayRetainTaskState, 'utf8');
				}
			}

			// set GDApplicationID in settings.json
			if (fs.existsSync(settingsJson)) {
				try {
					var settingsJsonData = fs.readFileSync(settingsJson, 'utf8');
					if (settingsJsonData.indexOf(packageName) == -1) {
						var applicationId = settingsJsonData.replace(/"GDApplicationID":""/g, '"GDApplicationID":"' + packageName + '"');
						chmod(770, settingsJson);
						fs.writeFileSync(settingsJson, applicationId, 'utf8');
					}
				} catch (err) {
					throw new Error('An error occurred during updating settings.json: ' + err);
				}
			} else {
				throw new Error('Unable to find settings.json');
			}
			
			// changes to MainActivity.java to use GDCordovaActivity instead of CordovaActivity
			var packageToPath = path.join.apply(null,packageName.split('.'));
			var packageFullPath = path.join(context.opts.projectRoot, 'platforms','android','src',packageToPath);
			var mainActivity = path.join(packageFullPath, 'MainActivity.java');
			if (fs.existsSync(mainActivity)) {
				try {	
					var mainActivityData = fs.readFileSync(mainActivity, 'utf8');
					if (mainActivityData.indexOf("GDCordovaActivity") == -1) {
						var gdCordovaActivityImport = mainActivityData.replace(/import org.apache.cordova.*;/g, ['import org.apache.cordova.*;','import com.good.gd.cordova.core.GDCordovaActivity;'].join(os.EOL));
						fs.writeFileSync(mainActivity, gdCordovaActivityImport, 'utf8');
						var cordovaActivity = gdCordovaActivityImport.replace(/extends CordovaActivity/g, 'extends GDCordovaActivity /*extends CordovaActivity*/');
						fs.writeFileSync(mainActivity, cordovaActivity, 'utf8');
					}

				} catch (err) {
					throw new Error('An error occurred during updating MainActivity.java: ' + err);
				}
			} else {
					throw new Error('Unable to find MainActivity.java');
			}
    	} catch (err) {
    		throw new Error('An error occurred during updating AndroidManifest.xml: ' + err);
    	}
    } else {
    	throw new Error('Unable to find AndroidManifest.xml');
    }
};