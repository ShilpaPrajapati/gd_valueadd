#!/usr/bin/env node

/*
 * (c) 2016 BlackBerry Limited. All rights reserved.
 */

require('shelljs/global');

module.exports = function (context) {
	var fs = context.requireCordovaModule('fs');
	var path = context.requireCordovaModule('path');

	// this hook will write the path to 'plugins' folder into gradle.properties file in order to be able to read this path and use in other hooks

	var pluginPathCLI = process.argv[4]; // should point to something like '.../cordova-plugin-bbd-base'
	var pluginDir = context.opts.plugin.dir;
	var gradlePropertiesPath = path.join(pluginDir, 'scripts', 'gradle', 'gradle.properties');

	chmod(660, gradlePropertiesPath);
	// make sure that we currently handle case when user adds Base plugin (runs 'cordova plugin add <path>/cordova-plugin-bbd-base')
	// we need to get the path to 'plugins' folder based on this
	if (pluginPathCLI.indexOf('cordova-plugin-bbd-base') >= 0) {
		var pluginsPath = path.resolve(path.join(pluginPathCLI, '..')); // full path to 'plugins' folder
		
		if (fs.existsSync(gradlePropertiesPath)) {
			var gradlePropertiesData = fs.readFileSync(gradlePropertiesPath, { encoding: 'utf8' });
			if (gradlePropertiesData.indexOf('bbdCordovaPluginsDir=') < 0) {
				gradlePropertiesData = gradlePropertiesData + '\nbbdCordovaPluginsDir=' + pluginsPath;
				fs.writeFileSync(gradlePropertiesPath, gradlePropertiesData, { encoding: 'utf8' });
			}
		}
	}
};